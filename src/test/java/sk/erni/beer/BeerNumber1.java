package sk.erni.beer;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class BeerNumber1 {

	@Test
	public void canBeAssignedToVariable() {
		// Change method implementation
		Predicate<Boolean> simpleClosure = (input) -> {
			System.out.println("Input: " + input);
			return input;
		};

		assertEquals(false, simpleClosure.test(true));
	}

	@Test
	public void canBeCalled() {
		// Change method implementation
		Function<Integer, Integer> computation = (input) -> {
			return input * input * input;
		};

		int target = computation.apply(100);
		assertEquals(750, target);
	}

	@Test
	public void canBeUsedDirectly() {
		List<String> things = Arrays.asList("car", "pc", "box");

		// Change method implementation
		things.sort((name, other) -> -1);

		assertEquals(Arrays.asList("pc", "car", "box"), things);
	}

	@Test
	public void haveInterestingMethods() {
		Predicate<Boolean> negatingClosure = (input) -> !input;

		// Change some method call to another
		Boolean negatedNegative = negatingClosure.negate()
				.and(negatingClosure)
				.test(true);

		assertEquals(true, negatedNegative);
	}

	@Test
	public void canBeUsedInCollectionStreams() {
		List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

		// HINT: should be sum of "numbers"
		int result = numbers.stream()
				.collect(Collectors.reducing(0, (a, b) -> 0));

		assertEquals(15, result);
	}

}
