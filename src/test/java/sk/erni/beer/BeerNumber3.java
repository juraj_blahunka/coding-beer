package sk.erni.beer;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class BeerNumber3 {

	@Test
	public void canBeUsedInFibonacciSomehow() {
		// HINT: Map.computeIfAbsent looks interesting
		Fibonacci compute = new Fibonacci();
		assertEquals(1L, compute.fibonacci(2));
		assertEquals(2L, compute.fibonacci(3));
		assertEquals(3L, compute.fibonacci(4));
		assertEquals(55L, compute.fibonacci(10));

		// SPOILER! If all else fails:
		// 		http://codebetter.com/matthewpodwysocki/2008/08/01/recursing-into-recursion-memoization/
	}

	class Fibonacci {

		Map<Integer, Long> memoizer = new HashMap<Integer, Long>() {{
			put(0, 0L);
			put(1, 1L);
		}};

		public long fibonacci(int x) {
			throw new UnsupportedOperationException();
		}

	}

}
