package sk.erni.beer;

import org.junit.Test;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BeerNumber2 {

	@Test
	public void canReferenceStaticMethods() {
		List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

		// Implement static method
		List<Integer> collected = numbers.stream()
				.filter(BeerNumber2::isGreaterThan3)
				.collect(Collectors.toList());

		assertEquals(2, collected.size());
		assertTrue(collected.contains(4));
		assertTrue(collected.contains(5));
	}

	private static boolean isGreaterThan3(int input) {
		throw new UnsupportedOperationException();
	}

	@Test
	public void canReferenceInstanceMethods() {
		List<String> salaryRates = Arrays.asList("1", "[..BEER..]", "2", "as", "3");

		// Implement both instance methods (isNumber, convert)
		// HINT: some method call is still missing
		List<Integer> collected = salaryRates.stream()
				.map(this::convert)
				.collect(Collectors.toList());

		assertEquals(Arrays.asList(1, 2, 3), collected);
	}

	private Boolean isNumber(String input) {
		throw new UnsupportedOperationException();
	}

	private Integer convert(String input) {
		throw new UnsupportedOperationException();
	}

	@Test
	public void canBeUsedInSwing() {
		JButton button = new JButton("Hello");

		// Rewrite to Closure
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Yey! I'm being performed!");
			}
		});

		button.doClick();
		assertEquals("This is my beer!", button.getName());
	}

	@Test
	public void canBeComposed() {
		// Implement closures
		BiFunction<Integer, Integer, Integer> add = null;
		Function<Integer, Integer> dec = null;
		Function<Integer, Integer> sq = null;

		int result = add.andThen(dec.andThen(sq)).apply(3, 10);
		assertEquals(144, result);
	}

}
